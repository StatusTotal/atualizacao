{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   Atualizador
  Descricao:   Projeto para atualizacao de sistemas da Status Total
  Author   :   Cristina
  Date:        03-out-2017
  Last Update:
------------------------------------------------------------------------------------------------------------------------}

program Atualizador;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  Winapi.Windows,
  Winapi.Messages,
  Winapi.ShellAPI,
  System.SysUtils,
  System.Classes,
  Vcl.Forms,
  Vcl.Dialogs;

function FindForm(F: String): Boolean;
var
  iCount: Integer;
  r: Boolean;
begin
  r := False;

  for iCount := 0 to Screen.FormCount -1 do
  begin
    if UpperCase(Screen.Forms[iCount].Name) = UpperCase(F) then
      r := True;
  end;

  Result := r;
end;

var
  sNomeArqAtu, sNomeArqTemp, sNomeArqOld,
  sNomeSistema, PathExec: String;
  lSubstituiOk: Boolean;
  iSistema: Integer;
  hApp: HWND;
begin
  try
    iSistema := StrToInt(ParamStr(1));
//    iSistema := 1;  --TESTE

    lSubstituiOk := False;

    sNomeArqAtu  := '';
    sNomeArqTemp := '';
    sNomeArqOld  := '';
    sNomeSistema := '';

    case iSistema of
      1:  //MONITORAMENTO
      begin
        sNomeArqAtu  := 'MONITORAMENTO.exe';
        sNomeArqTemp := 'MONITORAMENTO-update.exe';
        sNomeArqOld  := 'OldMonitoramento';
        sNomeSistema := 'MONITORAMENTO';

        //Encerra o aplicativo que chamou o atualizador
        hApp := FindWindow(PChar(sNomeSistema), nil);

        if hApp <> 0 then
          SendMessage(hApp, WM_SYSCOMMAND, SC_CLOSE, 0);
      end;
      2:  //SYNC_MONITORAMENTO
      begin
        sNomeArqAtu  := 'SYNC_MONITORAMENTO.exe';
        sNomeArqTemp := 'SYNC_MONITORAMENTO-update.exe';
        sNomeArqOld  := 'OldSync';
        sNomeSistema := 'MONITORAMENTO';

        //Encerra o aplicativo que chamou o atualizador
        hApp := FindWindow(PChar('SYNC_MONITORAMENTO'), nil);

        if hApp <> 0 then
          SendMessage(hApp, WM_SYSCOMMAND, SC_CLOSE, 0);
      end;
      3:  //RCPN
      begin
        sNomeArqAtu  := 'RCPN.exe';
        sNomeArqTemp := 'RCPN-update.exe';
        sNomeArqOld  := 'OldRCPN';
        sNomeSistema := 'RCPN';

        //Encerra o aplicativo que chamou o atualizador
        hApp := FindWindow(PChar(sNomeSistema), nil);

        if hApp <> 0 then
          SendMessage(hApp, WM_SYSCOMMAND, SC_CLOSE, 0);
      end;
      4:  //CENTRAL PROTESTO
      begin
        sNomeArqAtu  := 'CentralProtesto.exe';
        sNomeArqTemp := 'CentralProtesto-update.exe';
        sNomeArqOld  := 'OldCentral';
        sNomeSistema := 'CENTRAL PROTESTO';

        //Encerra o aplicativo que chamou o atualizador
        hApp := FindWindow('CentralProtesto', nil);

        if hApp <> 0 then
          SendMessage(hApp, WM_SYSCOMMAND, SC_CLOSE, 0);
      end;
    end;

    PathExec := ParamStr(2) + '\' + sNomeSistema + '\';
//    PathExec := 'C:\TOTAL\MONITORAMENTO\';  --TESTE

    //Renomeia o arquivo antigo
    if not RenameFile(PathExec + sNomeArqAtu,
                      PathExec + sNomeArqOld + '.old') then
    begin
      raise Exception.Create(PChar('N�o foi possivel renomear a vers�o atual.' + #13#10 +
                                   'Por favor, entre em contato com o Suporte.'));
    end
    else
    begin
      //Guarda o arquivo antigo na pasta OLD
      if not DirectoryExists(PathExec + 'OLD\') then
        CreateDir(PathExec + 'OLD\')
      else
      begin
        if FileExists(PathExec + 'OLD\' + sNomeArqOld + '.old') then
          DeleteFile(PathExec + 'OLD\' + sNomeArqOld + '.old');
      end;

      if FileExists(PathExec + sNomeArqOld + '.old') then
        CopyFile(PChar(PathExec + sNomeArqOld + '.old'),
                 PChar(PathExec + 'OLD\' + sNomeArqOld + '.old'),
                 False);

      //Excluir arquivo old da pasta do sistema
      if FileExists(PathExec + sNomeArqOld + '.old') then
        DeleteFile(PathExec + sNomeArqOld + '.old');

      //Renomeia o arquivo temporario com o nome do arquivo antigo, tornando-o o arquivo atual
      lSubstituiOk := RenameFile(PathExec + sNomeArqTemp,
                                 PathExec + sNomeArqAtu);

      if not lSubstituiOk then
      begin
        raise Exception.Create(PChar('N�o foi possivel renomear a vers�o baixada.' + #13#10 +
                                     'Por favor, entre em contato com o Suporte.'));
      end
      else
      begin
        //Apaga o arquivo temporario
        if FileExists(PathExec + sNomeArqTemp) then
          DeleteFile(PathExec + sNomeArqTemp);
      end;
    end;
  except
    on E: Exception do
    begin
      Writeln(E.ClassName, ': ', E.Message);
    end;
  end;

  //Abre o sistema
  if FileExists(PathExec + sNomeArqAtu) then
    ShellExecute(Application.Handle,
                 'open',
                 PChar(PathExec + sNomeArqAtu),
                 nil,
                 nil,
                 SW_SHOWNORMAL);
end.
